package com.testapplication.activities

import android.annotation.SuppressLint
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.testapplication.R
import com.testapplication.adapters.ContactsAdapter
import com.testapplication.api.ApiInterface
import com.testapplication.model.ContactsContainer
import com.testapplication.model.ContactsGroup
import org.androidannotations.annotations.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    @ViewById
    lateinit var searchView: SearchView
    @ViewById
    lateinit var contactsList: RecyclerView
    @ViewById
    lateinit var progressBar: ProgressBar

    var contactsContainer: ContactsContainer? = null


    /**
     * Method called after activity views created
     */
    @AfterViews
    fun afterViews() {
        contactsList.layoutManager = LinearLayoutManager(this)
        contactsList.adapter = ContactsAdapter()
        contactsList.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        loadContacts()
    }

    override fun onResume() {
        super.onResume()
        searchView.setOnQueryTextListener(this)
    }

    override fun onPause() {
        super.onPause()
        searchView.setOnQueryTextListener(null)

    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(text: String?): Boolean {
        filterUsers()
        return false
    }

    /**
     * Method for loading contact list from server
     */
    fun loadContacts() {
        ApiInterface.instance.getContacts().enqueue(object : Callback<ContactsContainer> {
            override fun onResponse(call: Call<ContactsContainer>?, response: Response<ContactsContainer>?) {
                hideProgressBar()
                if (response!!.isSuccessful) {
                    contactsContainer = response.body()
                    onContactsFetched()
                } else {
                    showError(getString(R.string.unable_to_download_contacts))
                }
            }

            override fun onFailure(call: Call<ContactsContainer>?, t: Throwable?) {
                hideProgressBar()
                showError(t?.message)
            }
        })
    }

    /**
     * When contacts loaded, showing them on screen
     */
    fun onContactsFetched() {
        filterUsers()
        searchView.visibility = View.VISIBLE
    }

    fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    /**
     * Show toast with string
     * @param message: Message to show
     */
    fun showError(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    /**
     * Filtering contacts by current keyword in SearchView
     */
    @Background
    fun filterUsers() {
        setAdapterData(contactsContainer?.copy()!!.filterContacts(searchView.query.toString()))
    }

    /**
     * Setting adapter data with filtered contacts
     */
    @UiThread
    fun setAdapterData(filteredGroups: List<ContactsGroup>) {
        (contactsList.adapter as ContactsAdapter).setData(filteredGroups)
    }
}
