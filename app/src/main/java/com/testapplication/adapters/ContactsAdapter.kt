package com.testapplication.adapters

import android.text.TextUtils
import android.util.SparseArray
import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.testapplication.R
import com.testapplication.model.ContactsGroup
import java.util.*

/**
 * This adapter represents contacts list with sections
 */
class ContactsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(), View.OnClickListener {

    private var contactsGroups: List<ContactsGroup> = arrayListOf()

    private var viewTypes: SparseArray<ContactsViewType>? = null
    private var headerExpandTracker: SparseIntArray? = null

    init {
        contactsGroups = ArrayList()
        viewTypes = SparseArray()
        headerExpandTracker = SparseIntArray()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val context = parent.context
        val holder: RecyclerView.ViewHolder
        holder = if (viewType == USER_TYPE) {
            UserViewHolder(LayoutInflater.from(context).inflate(R.layout.item_contact_list, parent, false))
        } else {
            SectionHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.layout_user_list_section_header,
                    parent,
                    false
                )
            )
        }
        return holder
    }

    /**
     * Binding ViewHolders
     * @param holder ViewHolder
     * @param position adapter position
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemViewType = getItemViewType(position)
        val viewType = viewTypes!!.get(position)
        if (itemViewType == USER_TYPE) {
            bindUserViewHolder(holder as UserViewHolder, viewType)
        } else {
            bindHeaderViewHolder(holder as SectionHolder, viewType)
        }
    }

    /**
     * Binding section headers
     */
    private fun bindHeaderViewHolder(holder: SectionHolder, viewType: ContactsViewType) {
        val section = viewType.section
        holder.itemView.tag = section
        holder.itemView.setOnClickListener(this)
        holder.txtTitle.text = contactsGroups[section].groupName?.capitalize()
    }

    /**
     * Binding contact item
     */
    private fun bindUserViewHolder(holder: UserViewHolder, viewType: ContactsViewType) {
        val section = viewType.section
        val childPosition = viewType.childPosition
        val user = contactsGroups[section].people[childPosition]
        holder.itemView.setOnClickListener(null)
        holder.txtUsername.text = user.getFullName()
        holder.txtStatus.text = user.statusMessage
        holder.txtStatus.visibility = if (TextUtils.isEmpty(user.statusMessage)) View.GONE else View.VISIBLE
        holder.txtUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(user.getStatusIconResId(), 0, 0, 0)
    }

    /**
     * Handler for section header click
     */
    override fun onClick(v: View) {
        val position = v.tag as Int
        if (headerExpandTracker!!.get(position) == 0) {
            // Collapsed. Now expand it
            headerExpandTracker!!.put(position, 1)
        } else {
            // Expanded. Now collapse it
            headerExpandTracker!!.put(position, 0)
        }
        onDatasetChanged()
    }


    /**
     * Calculating number of items in list
     */
    override fun getItemCount(): Int {
        var count = 0
        viewTypes!!.clear()
        for (i in contactsGroups.indices) {
            viewTypes!!.put(count++, ContactsViewType(i, -1, HEADER_TYPE))
            val contactsGroup = contactsGroups[i]
            val childCount = contactsGroup.people.size
            if (headerExpandTracker!!.get(i) != 0) {
                // Expanded State
                for (j in 0 until childCount) {
                    viewTypes!!.put(count++, ContactsViewType(i, j, USER_TYPE))
                }
            }
        }
        return count
    }

    /**
     * Determine list item type for position
     */
    override fun getItemViewType(position: Int): Int {
        return if (viewTypes!!.get(position).type == HEADER_TYPE) {
            HEADER_TYPE
        } else {
            USER_TYPE
        }
    }

    /**
     * Set list of contact groups
     */
    fun setData(contactsGroups: List<ContactsGroup>) {
        this.contactsGroups = contactsGroups
        headerExpandTracker = SparseIntArray(contactsGroups.size)
        for (i in contactsGroups.indices) {
            headerExpandTracker!!.put(i, 1)
        }
        onDatasetChanged()
    }

    /**
     * Notify list when dataset changed
     */
    private fun onDatasetChanged() {
        viewTypes = SparseArray()
        notifyDataSetChanged()
    }


    /**
     * ViewHolder represents Contact item
     */
    internal class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txtUsername: TextView = itemView.findViewById(R.id.txtUsername)
        var txtStatus: TextView = itemView.findViewById(R.id.txtStatus)

    }

    /**
     * ViewHolder represents Header item
     */
    internal class SectionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var txtTitle: TextView = itemView.findViewById(R.id.txtTitle)

    }

    /**
     * Container for current section and contact position in list
     * @param section Section for current list item
     * @param childPosition Contact position for current list item
     * @param type Current item ViewHolder type
     */
    internal class ContactsViewType(val section: Int, val childPosition: Int, val type: Int)

    /**
     * ViewHolder types for this adapter
     */
    companion object {
        private const val USER_TYPE = 1
        private const val HEADER_TYPE = 2
    }
}