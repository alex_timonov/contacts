package com.testapplication.api

import com.testapplication.BuildConfig
import com.testapplication.model.ContactsContainer
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.*

/**
 * Interface for server API calls
 */
interface ApiInterface {

    @GET("index.php/s/F5WttwCODi1z3oo/download?path=%2F&files=contacts.json")
    fun getContacts(): Call<ContactsContainer>

    companion object {
        val instance: ApiInterface by lazy {
            val loggingInterceptor = HttpLoggingInterceptor()
            if (BuildConfig.DEBUG) {
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            } else {
                loggingInterceptor.level = HttpLoggingInterceptor.Level.NONE
            }
            val client = OkHttpClient().newBuilder()
                .addInterceptor(loggingInterceptor.apply {})
                .connectionSpecs(
                    Arrays.asList(
                        ConnectionSpec.MODERN_TLS,
                        ConnectionSpec.CLEARTEXT,
                        ConnectionSpec.COMPATIBLE_TLS
                    )
                )
                .build()
            val retrofit = Retrofit.Builder()
                .baseUrl("https://file.wowapp.me/owncloud/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            retrofit.create(ApiInterface::class.java)
        }
    }
}