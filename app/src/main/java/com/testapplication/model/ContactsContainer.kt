package com.testapplication.model

import com.google.gson.annotations.SerializedName

/**
 * Class represents set of contact groups json data model
 */
data class ContactsContainer(
    @SerializedName("groups") val groups: List<ContactsGroup> = listOf()
) {

    /**
     * Method allows to copy ContactsContainer
     */
    fun copy(): ContactsContainer {
        val groupsClone: ArrayList<ContactsGroup> = ArrayList()
        groups.forEach { groupsClone.add(it.copy()) }
        return ContactsContainer(groupsClone)
    }


    /**
     * Method filters contacts by keyword
     * @param keyword keword
     */
    fun filterContacts(keyword: String?): List<ContactsGroup> {
        for (contactGroup in groups) {
            val mutablePeople = contactGroup.people.toMutableList()
            val peopleIterator = mutablePeople.iterator()
            peopleIterator.forEach {
                if (!it.usernameContainsKeyword(keyword)) {
                    peopleIterator.remove()
                }
            }
            contactGroup.people = mutablePeople

        }
        return groups
    }
}


