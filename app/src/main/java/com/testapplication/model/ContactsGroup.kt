package com.testapplication.model

import com.google.gson.annotations.SerializedName

/**
 * Class represents contact group json data model
 */
data class ContactsGroup(
    @SerializedName("groupName") val groupName: String? = null,
    @SerializedName("people") var people: List<User> = listOf()

)
