package com.testapplication.model

import com.google.gson.annotations.SerializedName
import com.testapplication.R


/**
 * Class represents User json data model
 */
class User {

    @SerializedName("firstName")
    var firstName: String? = null
    @SerializedName("lastName")
    var lastName: String? = null
    @SerializedName("statusIcon")
    var statusIcon: String? = null
    @SerializedName("statusMessage")
    var statusMessage: String? = null

    fun getFullName(): String {
        return String.format("%s %s", firstName, lastName)
    }

    /**
     * Method verifies if username contains keyword
     */
    fun usernameContainsKeyword(keyword: String?): Boolean {
        return keyword.isNullOrEmpty() || getFullName().trim().contains(keyword, true)
    }

    /**
     * Get drawable icon for status
     */
    fun getStatusIconResId(): Int {
        var drawable = 0

        when (statusIcon) {
            "away" -> {
                drawable = R.mipmap.contacts_list_status_away
            }
            "busy" -> {
                drawable = R.mipmap.contacts_list_status_busy
            }
            "online" -> {
                drawable = R.mipmap.contacts_list_status_online
            }
            "offline" -> {
                drawable = R.mipmap.contacts_list_status_offline
            }
            "pending" -> {
                drawable = R.mipmap.contacts_list_status_pending
            }
            "callforwarding" -> {
                drawable = R.mipmap.contacts_list_call_forward
            }
        }
        return drawable
    }
}